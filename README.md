# SustainGraph

ARSINOE SustainGraph is a Knowledge Graph developed to track information related to the evolution of targets defined in the United Nations Sustainable Development Goals (SDGs) at national and regional level. 

## Prerequisites

Install Neo4j Enterprise Edition 4.4.7 or Neo4j Community Edition 4.4.7 or Neo4j Desktop.


Install [neosemantics (n10s) 4.4.0.0](https://neo4j.com/labs/neosemantics/4.1/introduction/) plugin.  neosemantics plugin enables the use of RDF and its associated vocabularies like OWL, RDFS, SKOS, and others in Neo4j.

Install [APOC 4.4.0.3](https://neo4j.com/labs/apoc/) (Awesome Procedures on Neo4j) plugin.  APOC (Awesome Procedures on Neo4j) contains more than 450 procedures and functions providing functionality for utilities, conversions, graph updates, and more. We’re going to use this tool to scrape web pages and apply NLP techniques on text data.

Install python packages [py2neo](https://py2neo.org/2021.1/), which is a client library and toolkit for working with Neo4j from within Python applications and from the command line and [pycountry](https://pypi.org/project/pycountry/), that is library of python that provides the ISO databases for countries, subdivision of countries etc. Regarding the SDGs, install [eurostat](https://pypi.org/project/eurostat/) library, which read data from Eurostat website and [asyncio](https://docs.python.org/3/library/asyncio.html) for asynchronous HTTP Requests in Python.
```
pip install py2neo
pip install pycountry
pip install eurostat
pip install asyncio
```
If you want to install the aforementioned packages in the jupyter notebook directly, then go the corresponding section in the Wiki for details.

## Cite

To cite this work, please use:

Fotopoulou E, Mandilara I, Zafeiropoulos A, Laspidou C, Adamos G, Koundouri P and Papavassiliou S (2022) SustainGraph: A knowledge graph for tracking the progress and the interlinking among the sustainable development goals’ targets. Front. Environ. Sci. 10:1003599. doi: [10.3389/fenvs.2022.1003599](https://www.frontiersin.org/articles/10.3389/fenvs.2022.1003599/full)

## Contact

For any request for detailed information or expression of interest for participating at this initiative, you may contact:

- Anastasios Zafeiropoulos - tzafeir (at) cn (dot) ntua (dot) gr
- Eleni Fotopoulou - efotopoulou (at) netmode (dot) ntua (dot) gr
- Ioanna Mandilara - ioannamand (at) netmode (dot) ntua (dot) gr
- Christina Maria Androna - andronaxm (at) netmode (dot) ntua (dot) gr
